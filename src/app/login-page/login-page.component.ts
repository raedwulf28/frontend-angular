import { Component, OnInit } from '@angular/core';
import { AuthenticationService, AuthState } from '../services/authService/authentication.service';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent implements OnInit {
  user: SocialUser;
  loggedIn: boolean;
  checked = false;
  returnPath = '/homepage';

  constructor(
    public authService_: AuthenticationService,
    public authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    authService_.authChange.subscribe(
      newAuthState =>
      this.loggedIn = (newAuthState === AuthState.LoggedIn));
  }

  ngOnInit() {
    // Get the query params
    this.route.queryParams
      .subscribe(params => this.returnPath = params['return'] || '/homepage');
    this.authService.authState.subscribe((user) => {
      this.user = user;
      console.log(user);
    });
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then( () =>
   // jut for testing:
   this.login()
    );
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then( () =>
    // jut for testing:
    this.login()
    );
  }



  login(): void {
    this.authService_.login();
    this.router.navigateByUrl(this.returnPath);
  }

}

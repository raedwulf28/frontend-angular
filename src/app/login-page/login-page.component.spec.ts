import { LoginPageComponent } from './login-page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatIconModule, MatButtonModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { AuthenticationService } from '../services/authService/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub } from '../mocks/router-stubs';
import { AuthService, AuthServiceConfig } from 'angularx-social-login';
import { RouterTestingModule } from '@angular/router/testing';

let activatedRoute: ActivatedRouteStub;
const STUB = [];
const AuthServiceConfigStub = new AuthServiceConfig(STUB);

describe('LoginPageComponent', () => {

  let fixture: ComponentFixture<LoginPageComponent>;
  let component: LoginPageComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        AuthService,
        {provide: AuthServiceConfig, useValue: AuthServiceConfigStub},
        AuthenticationService,
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      imports: [
        FormsModule,
        MatCheckboxModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        RouterTestingModule
      ]
    });
    activatedRoute = new ActivatedRouteStub();
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;

  });

  it('should be able to create component instance', () => {
    expect(component).toBeDefined();
  });

});

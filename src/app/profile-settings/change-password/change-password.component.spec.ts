import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ChangePasswordComponent } from './change-password.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatIconModule, MatButtonModule } from '@angular/material';

describe('ChangePasswordComponent', () => {

  let fixture: ComponentFixture<ChangePasswordComponent>;
  let component: ChangePasswordComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
      ],
      declarations: [ChangePasswordComponent],
      imports: [
        FormsModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
      ]
    });

    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;

  });

  it('should be able to create component instance', () => {
    expect(component).toBeDefined();
  });

});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})

export class ChangePasswordComponent implements OnInit {
  showPassword = 'password';
  showCPassword = 'password';
  constructor() {

  }

  ngOnInit() {

  }

  toggleViewPassword() {
    if (this.showPassword === 'password') {
      this.showPassword = '';
    }else {
      this.showPassword = 'password';
    }
  }

  toggleViewCPassword() {
    if (this.showCPassword === 'password') {
      this.showCPassword = '';
    }else {
      this.showCPassword = 'password';
    }
  }
}

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Http, RequestOptions, HttpModule, XHRBackend, ConnectionBackend } from '@angular/http';
import { ProfileSettingsComponent } from './profile-settings.component';
import { ProfileSettingsRoutingModule } from './profile-settings-routing';
import { ProfileComponent } from '../profile/profile.component';
import { SharedModule } from '../../shared/shared.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NotificationsComponent } from '../notifications/notifications.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { RouterModule, Router, Routes } from '@angular/router';
import { RouteGuardService } from '../../services/route-guard/route-guard.service';

@NgModule({
  declarations: [
    ProfileSettingsComponent,
    ProfileComponent,
    NotificationsComponent,
    ChangePasswordComponent
],
  imports: [
    CommonModule,
    FlexLayoutModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ProfileSettingsRoutingModule,
  ],
  providers: [
    RouteGuardService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
   bootstrap: [ ProfileSettingsComponent ]
})
export class ProfileSettingsModule { }

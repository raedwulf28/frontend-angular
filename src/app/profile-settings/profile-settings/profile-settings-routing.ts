import { NgModule, ModuleWithProviders } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { ProfileSettingsComponent } from './profile-settings.component';
import { ProfileComponent } from '../profile/profile.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { RouteGuardService } from '../../services/route-guard/route-guard.service';

const appRoutes: Routes = [
    { path: '', component: ProfileSettingsComponent,
    canActivate: [RouteGuardService],
    canActivateChild: [RouteGuardService],
    children: [
      {
        path: 'account',
        component: ProfileComponent
      },
      {
        path: 'changePassword',
        component: ChangePasswordComponent
      },
      {
        path: 'notifications',
        component: NotificationsComponent
      },
      {
        path: '',
        component: ProfileComponent
      }
    ]}];

@NgModule({
  imports: [
    RouterModule.forChild(
      appRoutes,
//      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})

export class ProfileSettingsRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Lesson } from '../shared/models/lesson';

@Component({
  selector: 'app-my-tutorials',
  templateUrl: './my-tutorials.component.html',
  styleUrls: ['./my-tutorials.component.scss']
})
export class MyTutorialsComponent implements OnInit {
  lessonsArray: Lesson[] = [];
  constructor() { }

  ngOnInit() {
  for (let i = 0; i < 3; i++) {
    this.lessonsArray.push(
      {
        image: '../../assets/education.jpg',
        tutorName: 'testing',
        prepaid: { hours: 1, minutes: 30 },
        pricePerHour: 3,
        currency: '$',
        actions: ['messages', 'curriculum', 'buy hours'],
      }
    );
  }
  }

  actionAvailable(lesson: Lesson, action: string): boolean {
    if (lesson.actions && lesson.actions.find((available: string) => available === action)) {

      return true;
    }

    return false;
  }

  testing(lesson: Lesson): void {
    if (lesson.actions.length > 1) {
      lesson.actions = ['messages'];
    }else {
      lesson.actions = ['messages', 'curriculum', 'buy hours'];
    }
  }

}


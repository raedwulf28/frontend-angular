import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HomePageComponent } from './home-page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UrlRouteService } from '../services/url-route/url-route.service';
import { MatGridListModule, MatCardModule, MatExpansionModule, MatButtonModule, MatIconModule } from '@angular/material';
import { ActivatedRouteStub } from '../mocks/router-stubs';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


const URL_CONST = false;
let activatedRoute: ActivatedRouteStub;


class UrlRouteServiceStub {
  public inHomePage(): Observable<boolean> {
    return Observable.of(URL_CONST);
  }
  public outHomePage(): Observable<boolean> {
    return Observable.of(URL_CONST);
  }
}


describe('HomePageComponent', () => {

  let fixture: ComponentFixture<HomePageComponent>;
  let component: HomePageComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {provide: UrlRouteService, useClass: UrlRouteServiceStub},
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      declarations: [HomePageComponent],
      imports: [
        BrowserAnimationsModule,
        MatIconModule,
        MatButtonModule,
        MatGridListModule,
        MatExpansionModule,
        MatCardModule,
        RouterTestingModule
      ]
    });
    activatedRoute = new ActivatedRouteStub();
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;

  });

  it('should be able to create component instance', () => {
    expect(component).toBeDefined();
  });

});

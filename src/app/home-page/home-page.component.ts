import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { UrlRouteService } from '../services/url-route/url-route.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('grown', [
      state('in', style({transform: 'translateX(0)'})),
      transition('* => *', [
        animate(900, keyframes([
          style({opacity: 0, height: '0%' , offset: 0}),
          style({opacity: 1, height: '30%' ,  offset: 0.3}),
          style({opacity: 1, height: '100%' ,     offset: 1.0})
        ]))
      ])
    ])
  ]
})

export class HomePageComponent implements OnInit, OnDestroy {
// professions items for tests
  professions = [{img: '../../assets/crogs.jpg', title: 'Engineering'},
   {img: '../../assets/profile.jpg', title: 'Legals'},
   {img: '../../assets/computer.jpg', title: 'Advertising'},
   {img: '../../assets/chat.jpg', title: 'Languages'},
   {img: '../../assets/stadistics.jpg', title: 'Accountants'},
   {img: '../../assets/education.jpg', title: 'Education'},
   {img: '../../assets/moneyBag.jpg', title: 'Business'},
   {img: '../../assets/house.jpg', title: 'Architecture'}];
  leftprofessions = [10, 11, 12, 13, 14, 15];
  panel= false;
@Input() state= 'in';
  language = '';
  constructor(public urlRouteService: UrlRouteService) {

   this.language = sessionStorage['language'];
  }


  ngOnInit() {
      // Set url out of Home Page
     this.urlRouteService.inHomePage();
  }

  // Set url out of Home Page
  ngOnDestroy() {
   this.urlRouteService.outHomePage();
  }


}

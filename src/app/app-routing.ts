import { NgModule, ModuleWithProviders } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { MyRequestsComponent } from './my-requests/my-requests.component';
import { MyTutorialsComponent } from './my-tutorials/my-tutorials.component';
import { BecomeTutorComponent } from './become-tutor/become-tutor.component';
import { RouteGuardService } from './services/route-guard/route-guard.service';
import { ProfileTutorComponent } from './profile-tutor/profile-tutor.component';
import { ErrorComponent } from './shared-component/error/error.component';
import { SearchResultComponent } from './search-result/search-result.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'homepage', pathMatch: 'full'},
    { path: 'homepage', component: HomePageComponent},
    { path: 'login', component: LoginPageComponent },
    { path: 'signup', component: SignupPageComponent },
    { path: 'my-requests', component: MyRequestsComponent, canActivate: [RouteGuardService] },
    { path: 'my-tutorials', component: MyTutorialsComponent, canActivate: [RouteGuardService] },
    { path: 'become-tutor', component: BecomeTutorComponent },
    { path: 'test', component: ProfileTutorComponent },
    { path: 'search-result', component: SearchResultComponent },
    { path: 'settings', loadChildren: '../app/profile-settings/profile-settings/profile-settings.module#ProfileSettingsModule' },
    { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
//      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}

import { TRANSLATIONS, TRANSLATIONS_FORMAT, LOCALE_ID } from '@angular/core';

export function getTranslationProviders(): Promise<any[]> {

  // Get the locale id from the global
  let locale: string;
  if (sessionStorage['language']) {
    locale = sessionStorage['language'];
  } else {
    locale = window.navigator.language as string;
    sessionStorage['language'] = locale;
  }

  // return no providers if fail to get translation file for locale
  const noProviders: Object[] = [];
  // No locale or U.S. English: no translation providers
  if (!locale || locale === 'en-US') {
    return Promise.resolve(noProviders);
  }
  // Ex: 'locale/messages.es.xlf`
  //  const translationFile = `./locale/messages.${locale}.xlf`;
  return getTranslationsWithSystemJs(locale)
    .then( (translations: string ) => [
      { provide: TRANSLATIONS, useValue: translations },
      { provide: TRANSLATIONS_FORMAT, useValue: 'xlf' },
      { provide: LOCALE_ID, useValue: locale }
    ])
    .catch(() => noProviders); // ignore if file not found
}

declare var System: any;

function getTranslationsWithSystemJs(loc: string) {
  return System.import('raw-loader!' + '../locale/messages.' + loc + '.xlf'); // relies on text plugin
}

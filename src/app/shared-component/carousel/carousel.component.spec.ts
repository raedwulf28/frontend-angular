import { NO_ERRORS_SCHEMA } from "@angular/core";
import { CarouselComponent } from "./carousel.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";

describe("CarouselComponent", () => {

  let fixture: ComponentFixture<CarouselComponent>;
  let component: CarouselComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
      ],
      declarations: [CarouselComponent]
    });

    fixture = TestBed.createComponent(CarouselComponent);
    component = fixture.componentInstance;

  });

  it("should be able to create component instance", () => {
    expect(component).toBeDefined();
  });
  
});

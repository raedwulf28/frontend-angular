import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('* => in', [
        animate(300, keyframes([
          style({opacity: 0, transform: 'translateX(-30%)', offset: 0}),
          style({opacity: 1, transform: 'translateX(15px)',  offset: 0.3}),
          style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
        ]))
      ]),
      transition('* => out', [
        animate(300, keyframes([
          style({opacity: 0, transform: 'translateX(30%)', offset: 0}),
          style({opacity: 1, transform: 'translateX(-15px)',  offset: 0.3}),
          style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
        ]))
      ])
    ])
  ]
})


export class CarouselComponent implements OnInit {
cont = 0;
@Input() state = 'in';
// IMAGES array where it´s the url,title, and img path
imagesT = [
  { 'title': 'We are covered', 'state': '../../../assets/logo.jpg', 'url': 'https://angular.io' },
  { 'title': 'Generation Gap', 'state': '../../../assets/sports-q-c-200-200-1.jpg', 'url': 'http://www.lorempixel.com'},
  { 'title': 'Best Me', 'state': '../../../assets/sports-q-c-200-200-9.jpg', 'url': 'http://www.google.com'},
];
@Input() images: any[]= [];
  constructor() {

  }

  ngOnInit() {
    this.cont = 0;
    this.images[0] = this.imagesT[0];
  }

/**
*   Method to change the view on carousel to the next image
*/
  nextimg() {
    this.state = '';
    if (this.cont < this.imagesT.length - 1) {
      ++this.cont;
    }else {
      this.cont = 0;
    }
    this.images[0] = this.imagesT[this.cont];
    this.state = 'in';
  }

/**
*   Method to change the view on carousel to the previuos image
*/
  previmg() {
    this.state = '';
    if (this.cont > 0) {
        --this.cont;
      }else {
        this.cont = this.imagesT.length - 1;
      }
    this.images[0] = this.imagesT[this.cont];
    this.state = 'out';
  }
}

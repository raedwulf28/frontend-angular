import { NO_ERRORS_SCHEMA } from '@angular/core';
import { QuestionBarComponent } from './question-bar.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';

describe('QuestionBarComponent', () => {

  let fixture: ComponentFixture<QuestionBarComponent>;
  let component: QuestionBarComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
      ],
      declarations: [QuestionBarComponent]
    });

    fixture = TestBed.createComponent(QuestionBarComponent);
    component = fixture.componentInstance;

  });

  it('should be able to create component instance', () => {
    expect(component).toBeDefined();
  });

});

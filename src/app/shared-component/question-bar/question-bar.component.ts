import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-question-bar',
  templateUrl: './question-bar.component.html',
  styleUrls: ['./question-bar.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('true', style({transform: 'translateX(0)'})),
      transition('false => true', [
        animate(50, keyframes([
          style({transform: 'translateX(30%)', offset: 0}),
          style({transform: 'translateX(-15px)',  offset: 0.3}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ]))
      ])
    ])
  ]
})

export class QuestionBarComponent implements OnInit {
  showFiller = false;
  language: string;
  constructor() {
    this.language = sessionStorage['language'];
  }

  ngOnInit() {

  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {
  range = new Date(2018, 7, 20);
  timeSelected = 'UTC GMT +0:00';
  timesMond = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  timesTues = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  timesWed = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  timesThur = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  timesFri = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  timesSat = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  timesSun = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  times = [new Date(0), new Date(0), new Date(0), new Date(0), new Date(0), new Date(0)];
  // this.hour.setTime(this.hour.getTime() + (1 * 60 * 60 * 1000))
  timeutc = ['UTC GMT +0:00', 'UTC GMT +1:00'];
  constructor() { }

  ngOnInit() {
  }

  nextWeek() {
    this.range = this.plusDays(7);
  }

  beforeWeek() {
    this.range = this.minusDays(7);
  }

  plusDays(days: number) {
    const aux = new Date(this.range.getTime() + (days * 86400000));
    return aux;
  }

  minusDays(days: number) {
    const aux = new Date(this.range.getTime() - (days * 86400000));
    return aux;
  }
}

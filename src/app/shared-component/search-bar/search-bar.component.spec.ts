import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SearchBarComponent } from "./search-bar.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";

describe("SearchBarComponent", () => {

  let fixture: ComponentFixture<SearchBarComponent>;
  let component: SearchBarComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
      ],
      declarations: [SearchBarComponent]
    });

    fixture = TestBed.createComponent(SearchBarComponent);
    component = fixture.componentInstance;

  });

  it("should be able to create component instance", () => {
    expect(component).toBeDefined();
  });
  
});

import { Component, OnInit } from '@angular/core';
import { ClockService } from '../../services/clock/clock.service';
import { AuthenticationService, AuthState } from '../../services/authService/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { UrlRouteService } from '../../services/url-route/url-route.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})

export class TopMenuComponent implements OnInit, OnChanges {
  time: any;
  title = 'Sotutors App';
  login = false;
  spanishSelected: boolean;
  englishSelected: boolean;
  subscription: Subscription;
  public url: string;
  homeStyle = false;
  language: string;

  constructor(
    private clockService: ClockService,
    public authService_: AuthenticationService,
    public urlRouteService: UrlRouteService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    authService_.authChange.subscribe(
      newAuthState => {
      this.login = (newAuthState === AuthState.LoggedIn);
       });
  }

  ngOnInit() {
    this.subscription = this.urlRouteService.inHomePage().subscribe( message => {
      setTimeout(() => {this.homeStyle = message; } , 1);
    });

    this.clockService.getClock().subscribe(time => this.time = time);
    if (sessionStorage['language'] && sessionStorage['language'] === 'es') {
      this.language = 'ESPAÑOL';
      this.spanishSelected = true;
      this.englishSelected = false;
    } else {
      this.language = 'ENGLISH, USD';
      this.spanishSelected = false;
      this.englishSelected = true;
    }

  }

  ngOnChanges() {
  }



  spanishLanguage() {
    sessionStorage['language'] = 'es';
    this.language = 'ESPAÑOL';
    location.reload();
  }

  englishLanguage() {
    sessionStorage['language'] = 'en';
    this.language = 'ENGLISH, USD';
    location.reload();
  }

  logout(): void {
    this.authService_.logout();
    this.router.navigateByUrl('/homepage');
  }
}

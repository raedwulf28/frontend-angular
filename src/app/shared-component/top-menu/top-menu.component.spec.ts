import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TopMenuComponent } from './top-menu.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatMenuModule } from '@angular/material';
import { UrlRouteService } from '../../services/url-route/url-route.service';
import { AuthenticationService } from '../../services/authService/authentication.service';
import { ClockService } from '../../services/clock/clock.service';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub } from '../../mocks/router-stubs';
import {RouterTestingModule} from '@angular/router/testing';
import { AuthService, AuthServiceConfig } from 'angularx-social-login';


const STUB = [];
const AuthServiceConfigStub = new AuthServiceConfig(STUB);



let activatedRoute: ActivatedRouteStub;

describe('TopMenuComponent', () => {

  let fixture: ComponentFixture<TopMenuComponent>;
  let component: TopMenuComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        ClockService,
        AuthenticationService,
        UrlRouteService,
        AuthService,
        {provide: AuthServiceConfig, useValue: AuthServiceConfigStub},
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      declarations: [TopMenuComponent],
      imports: [
        RouterTestingModule,
        MatMenuModule
      ]
    });
    activatedRoute = new ActivatedRouteStub();
    fixture = TestBed.createComponent(TopMenuComponent);
    component = fixture.componentInstance;

  });

  it('should be able to create component instance', () => {
    expect(component).toBeDefined();
  });

});

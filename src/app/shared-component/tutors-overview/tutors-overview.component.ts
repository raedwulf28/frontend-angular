import { Component, OnInit, Input } from '@angular/core';
import { TutorProfile } from '../../shared/models/tutor-profile';

@Component({
  selector: 'app-tutors-overview',
  templateUrl: './tutors-overview.component.html',
  styleUrls: ['./tutors-overview.component.scss']
})
export class TutorsOverviewComponent implements OnInit {
@Input() profile: TutorProfile;

  constructor() { }

  ngOnInit() {
  }

}

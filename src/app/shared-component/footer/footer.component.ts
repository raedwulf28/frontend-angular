import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})

export class FooterComponent implements OnInit {
  starsGold = [0, 1, 2, 3];
  starsNoGold = [0];
  constructor() {

  }
  ngOnInit() {
  }
}

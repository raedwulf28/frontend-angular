import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})
export class RateComponent implements OnInit {

  skills = [
    {name: 'Knowledge',
    stars: 5},
    {name: 'Flexibility',
    stars: 5},
    {name: 'Methodology',
    stars: 5},
    {name: 'Punctuality',
    stars: 5}];
  generalRate = (Math.round(5 * 100) / 100).toFixed(1);
  stars: any[];

  constructor() { }

  ngOnInit() {
    this.stars = this.fillStars(this.skills);
  }

 fillStars(skills: any): any {
    const result = [];
    for (let i = 0; i < skills.length; i++) {
      result[i] = [];
      for (let j = 0; j < skills[i].stars; j++) {
        result[i].push(true);
      }
    }
    return result;
  }

}

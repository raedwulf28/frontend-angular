import { Component, OnInit, Input } from '@angular/core';
import { Review } from '../../shared/models/review';

@Component({
  selector: 'app-short-review',
  templateUrl: './short-review.component.html',
  styleUrls: ['./short-review.component.scss']
})
export class ShortReviewComponent implements OnInit {
  @Input() review: Review = {rate: 0, comment: '', image: ''};
  reviewFixed: string;
  stars: any[];
  constructor() { }

  ngOnInit() {
    this.stars = this.fillStars(this.review.rate);
    this.reviewFixed = (Math.round(this.review.rate * 100) / 100).toFixed(1);
  }

 fillStars(reviewFixed: any): any {
    const result = [];
    for (let i = 0; i < reviewFixed; i++) {
        result.push(true);
    }
    return result;
  }
}

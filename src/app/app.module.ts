import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { TopMenuComponent } from './shared-component/top-menu/top-menu.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AppRoutingModule } from './app-routing';
import { SearchBarComponent } from './shared-component/search-bar/search-bar.component';
import { CarouselComponent } from './shared-component/carousel/carousel.component';
import { FooterComponent } from './shared-component/footer/footer.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { LoginPageComponent } from './login-page/login-page.component';
import { SignupPageComponent } from './signup-page/signup-page.component';
import { ClockService } from './services/clock/clock.service';
import { HttpWrapperFactory, HttpWrapperService } from './services/http-wrapper/http-wrapper.service';
import { RequestOptions, HttpModule, XHRBackend } from '@angular/http';
import { MyRequestsComponent } from './my-requests/my-requests.component';
import { MyTutorialsComponent } from './my-tutorials/my-tutorials.component';
import { ProfileSettingsModule } from './profile-settings/profile-settings/profile-settings.module';
import { SharedModule } from './shared/shared.module';
import { AuthenticationService } from './services/authService/authentication.service';
import { UrlRouteService } from './services/url-route/url-route.service';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { BecomeTutorComponent } from './become-tutor/become-tutor.component';
import { RateComponent } from './shared-component/rate/rate.component';
import { ProfileTutorComponent } from './profile-tutor/profile-tutor.component';
import { ErrorComponent } from './shared-component/error/error.component';
import { ShortReviewComponent } from './shared-component/short-review/short-review.component';
import { ScheduleComponent } from './shared-component/schedule/schedule.component';
import { DataService } from './services/data/Data.service';
import { SearchResultComponent } from './search-result/search-result.component';
import { TutorsOverviewComponent } from './shared-component/tutors-overview/tutors-overview.component';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('Put here the ID of Google project')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('Put here the ID of Facebook App')
  }
]);

@NgModule({
  declarations: [
    ScheduleComponent,
    ShortReviewComponent,
    RateComponent,
    ErrorComponent,
    ProfileTutorComponent,
    AppComponent,
    TopMenuComponent,
    HomePageComponent,
    SearchBarComponent,
    CarouselComponent,
    FooterComponent,
    LoginPageComponent,
    SignupPageComponent,
    MyRequestsComponent,
    MyTutorialsComponent,
    BecomeTutorComponent,
    SearchResultComponent,
    TutorsOverviewComponent
],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    ProfileSettingsModule,
    SharedModule,
    HttpClientModule,
    HttpClientJsonpModule,
    SocialLoginModule.initialize(config)
  ],
  providers: [
    ClockService,
    AuthenticationService,
    AuthService,
    UrlRouteService,
    DataService,
    {
      provide: HttpWrapperService,
      useFactory: HttpWrapperFactory,
      deps: [XHRBackend, RequestOptions]
     }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }

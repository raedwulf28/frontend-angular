import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatTable} from '@angular/material';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.scss']
})
export class MyRequestsComponent implements OnInit {
  displayedColumns = ['tutor', 'message', 'date', 'actions'];
  dataSource = new MatTableDataSource<Element>(ELEMENT_DATA);
  @ViewChild(MatTable) table: MatTable<Element>;

  constructor() { }

  ngOnInit() {
    console.log(this.dataSource);
  }

  click() {
    this.dataSource.data.push(
      {image: '../../assets/education.jpg', tutor: 'testing2', message: 'testing', date: new Date(), check: true, actions: []}
    );
    console.log(this.dataSource);
    this.table.renderRows();
  }

}

export interface Element {
  image: string;
  tutor: string;
  message: string;
  date: Date;
  check: boolean;
  actions: string[];
}

const ELEMENT_DATA: Element[] = [
  {image: '../../assets/education.jpg', tutor: 'testing', message: 'testing', date: new Date(), check: true, actions: []}
];

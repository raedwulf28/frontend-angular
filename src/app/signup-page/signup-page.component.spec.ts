import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SignupPageComponent } from './signup-page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule, MatIconModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ActivatedRouteStub } from '../mocks/router-stubs';
import { AuthServiceConfig } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { AuthenticationService } from '../services/authService/authentication.service';
import { ActivatedRoute } from '@angular/router';

let activatedRoute: ActivatedRouteStub;
const STUB = [];
const AuthServiceConfigStub = new AuthServiceConfig(STUB);

describe('SignupPageComponent', () => {

  let fixture: ComponentFixture<SignupPageComponent>;
  let component: SignupPageComponent;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        AuthService,
        {provide: AuthServiceConfig, useValue: AuthServiceConfigStub},
        AuthenticationService,
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      declarations: [SignupPageComponent],
      imports: [ MatButtonModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
       ],
    });

    fixture = TestBed.createComponent(SignupPageComponent);
    component = fixture.componentInstance;

  });

  it('should be able to create component instance', () => {
    expect(component).toBeDefined();
  });
});

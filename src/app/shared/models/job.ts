export class Job {
  company: string;
  position: string;
  startTime: Date;
  endTime: Date;

  constructor(job: any) {
    this.company = job.company;
    this.position = job.position;
    this.startTime = job.startTime;
    this.endTime = job.endTime;
  }
}

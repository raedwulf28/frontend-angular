export class EducationHistory {
  title: string;
  startYear: number;
  endYear: number;
  description: string;
}

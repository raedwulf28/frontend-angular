import { Experience } from './experience';
import { WorkExperience } from './work-history';
import { EducationHistory } from './education-history';
import { Skill } from './skill';
import { SafeResourceUrl } from '@angular/platform-browser';

export class TutorProfile {
  img: string;
  name: string;
  city: string;
  tutorIn: string;
  speakIn: string;
  experience: Experience;
  tutorDescription: string;
  reviews: Array<any>;
  payRate: number;
  workExperiences: WorkExperience[];
  educations: EducationHistory[];
  skills: Skill[];
  certifications: string[];
  videoUrl: SafeResourceUrl;

  constructor(tutorProfile: any) {
    this.img = tutorProfile.img;
    this.name = tutorProfile.name;
    this.city = tutorProfile.city;
    this.tutorIn = tutorProfile.tutorIn;
    this.speakIn = tutorProfile.speakIn;
    this.payRate = tutorProfile.payRate;
    this.experience = tutorProfile.experience;
    this.tutorDescription = tutorProfile.tutorDescription;
    this.workExperiences = tutorProfile.workExperiences;
    this.educations = tutorProfile.educations;
    this.skills = tutorProfile.skills;
    this.videoUrl = tutorProfile.videoUrl;
    this.reviews = tutorProfile.reviews;
    this.certifications = tutorProfile.certifications;
  }

}

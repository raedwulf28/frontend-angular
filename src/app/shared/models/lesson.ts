import { Time } from '@angular/common';

export class Lesson {
  image: string;
  tutorName: string;
  prepaid: Time;
  pricePerHour: number;
  currency: string;
  actions: Array<string>;

  constructor(lesson: any) {
    this.image = lesson.image;
    this.tutorName = lesson.tutorName;
    this.prepaid = lesson.prepaid;
    this.pricePerHour = lesson.pricePerHour;
    this.currency = lesson.currency;
    this.actions = lesson.actions;
  }
}

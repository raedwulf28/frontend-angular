export class Review {
  rate: number;
  comment: string;
  image: string;

  constructor(review: any) {
    this.rate = review.rate;
    this.comment = review.comment;
    this.image = review.image;
  }
}

export class WorkExperience {
  title: string;
  startYear: number;
  endYear: number;
  description: string;
}

import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {MatChipInputEvent} from '@angular/material';
import {ENTER, COMMA} from '@angular/cdk/keycodes';
import { TutorProfile } from '../shared/models/tutor-profile';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data/Data.service';

@Component({
  selector: 'app-profile-tutor',
  templateUrl: './profile-tutor.component.html',
  styleUrls: ['./profile-tutor.component.scss']
})
export class ProfileTutorComponent implements OnInit {

  profile = new TutorProfile(
    {
      img: '../../assets/profile-tutor.jpg',
      name: 'Allison Costa',
      city: 'Texas,Austin',
      tutorIn:  'Legal case',
      speakIn: 'english, spanish',
      experience: {lessons: 265, years: 2},
      payRate: 30,
      tutorDescription: `Hello everyone,\n\n
        I am a teacher that is currently teaching English and French to adults in the North of France.
        \n I worked in UK and USA for 3 years where I was teaching English to young French students.
        All my lessons are interactive, new and with games. I adapt the content to you and your progress :)
          \n I love to travel and meet new people so please don't hesitate!`,
      workExperiences: [{
        title: 'Legal Cases',
        startYear: 2015,
        endYear: 2018,
        description: 'DEUG ANGLAIS'
      }],
      educations: [{
          title: 'Université de LILLE 3',
          startYear: 2015,
          endYear: 2018,
          description: 'DEUG ANGLAIS'
        },
        {
          title: 'Université de LILLE 3',
          startYear: 2015,
          endYear: 2018,
          description: 'DEUG ANGLAIS'
        }
      ],
      skills: [
        { name: 'Laboral' },
        { name: 'Legal' },
        { name: 'Family Case' },
        { name: 'Notary' }
      ],
      videoUrl: '',
      reviews: [],
      certifications: ['']
    }
  );

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  editable = false;
  separatorKeysCodes = [ENTER, COMMA];
  edition: boolean;

  constructor(
    private sanitizer: DomSanitizer,
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute) {
   }

  ngOnInit() {

    // testing purpose
    this.profile.reviews.push({rate: 5, comment: 'i Recommend this Tutor',
      image: 'https://www.webconsultas.com/sites/default/files/styles/encabezado_articulo/public/articulos/perfil-resilencia.jpg'});
    this.profile.reviews.push({rate: 5, comment: 'Hello it was a great Tutor',
      image: 'http://como-encontrar-parejas.es/wp-content/uploads/foto-de-perfil.jpg'});
    this.profile.videoUrl = this.updateVideoUrlYoutube( 'n4uDmaSrkAY');

    this.route
    .queryParams
    .subscribe(params => {
      console.log(this.dataService.serviceData);
      if (params['editionFromForm'] && this.dataService.serviceData) {

        this.edition = true;

        if (this.dataService.serviceData.videoUrl) {
          this.profile.videoUrl = this.dataService.serviceData.videoUrl.includes('watch?v=') ?
          this.updateVideoUrlYoutube(this.dataService.serviceData.videoUrl.split('watch?v=')[1]) :
          this.updateVideoUrlYoutube('Error');
        } else {
          this.profile.videoUrl = this.updateVideoUrlYoutube('Error');
        }

        this.profile.name = this.dataService.serviceData.name || 'No Info';
        this.profile.city = this.dataService.serviceData.city || 'No Info';
        this.profile.tutorIn = this.dataService.serviceData.tutorIn || 'No Info';
        this.profile.speakIn = this.dataService.serviceData.speakIn || 'No Info';
        this.profile.payRate = this.dataService.serviceData.payRate || 0;
        this.profile.tutorDescription = this.dataService.serviceData.tutorDescription || 'No Info';
        this.profile.certifications = this.dataService.serviceData.certifications || [];
        this.profile.workExperiences = this.dataService.serviceData.workHistory || [];
        this.profile.educations = [
          {title: this.dataService.serviceData.education.institute,
          description: this.dataService.serviceData.education.degree,
          endYear: 0,
          startYear: 0}] || [];
        this.profile.img = this.dataService.serviceData.image || '../../assets/profile-tutor.jpg';

      }
    });

  }

  updateVideoUrlYoutube(id: string): SafeResourceUrl {
    const dangerousVideoUrl = 'https://www.youtube.com/embed/' + id;
    return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.profile.skills.push({ name: value.trim() });
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: any): void {
    const index = this.profile.skills.indexOf(fruit);

    if (index >= 0) {
      this.profile.skills.splice(index, 1);
    }
  }

  edit() {
    this.dataService.serviceData = {
      firstName: this.profile.name.split(' ')[0],
      LastName: this.profile.name.split(' ')[1],
      city: this.profile.city,
      tutorIn: this.profile.tutorIn,
      speakIn: this.profile.speakIn,
      payRate: this.profile.payRate,
      tutorDescription: this.profile.tutorDescription,
      certifications: this.profile.certifications,
      workHistory: this.profile.workExperiences,
      institute: this.profile.educations[0].description,
      degree: this.profile.educations[0].title,
    };
    this.router.navigate(['/become-tutor'], {queryParams: {editionFromResult: true}});
  }
}

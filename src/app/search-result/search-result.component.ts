import { Component, OnInit } from '@angular/core';
import { TutorProfile } from '../shared/models/tutor-profile';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
  search = '';
  totalresult = 1242;
  items = [0,1,2,3,4,5,6];
  demoProfile = new TutorProfile(
    {
      img: '../../assets/profile-tutor.jpg',
      name: 'Allison Costa',
      city: 'Texas,Austin',
      tutorIn:  'Legal case',
      speakIn: 'english, spanish',
      experience: {lessons: 265, years: 2},
      payRate: 30,
      tutorDescription: `Hello everyone,\n\n
        I am a teacher that is currently teaching English and French to adults in the North of France.
        \n I worked in UK and USA for 3 years where I was teaching English to young French students.
        All my lessons are interactive, new and with games. I adapt the content to you and your progress :)
          \n I love to travel and meet new people so please don't hesitate!`,
      workExperiences: [{
        title: 'Legal Cases',
        startYear: 2015,
        endYear: 2018,
        description: 'DEUG ANGLAIS'
      }],
      educations: [{
          title: 'Université de LILLE 3',
          startYear: 2015,
          endYear: 2018,
          description: 'DEUG ANGLAIS'
        },
        {
          title: 'Université de LILLE 3',
          startYear: 2015,
          endYear: 2018,
          description: 'DEUG ANGLAIS'
        }
      ],
      skills: [
        { name: 'Laboral' },
        { name: 'Legal' },
        { name: 'Family Case' },
        { name: 'Notary' }
      ],
      videoUrl: '',
      reviews: [],
      certifications: ['']
    }
  );
  constructor(
    private sanitizer: DomSanitizer
    ) { }

  ngOnInit() {
    this.demoProfile.videoUrl = this.updateVideoUrlYoutube( 'n4uDmaSrkAY');
  }

  updateVideoUrlYoutube(id: string): SafeResourceUrl {
    const dangerousVideoUrl = 'https://www.youtube.com/embed/' + id;
    return this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
  }


}

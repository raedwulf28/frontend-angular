import { TestBed, async, inject } from '@angular/core/testing';
import { UrlRouteService } from './url-route.service';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub } from '../../mocks/router-stubs';
import { RouterTestingModule } from '@angular/router/testing';



let activatedRoute: ActivatedRouteStub;

describe('Service: UrlRoute', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlRouteService,
        {provide: ActivatedRoute, useValue: activatedRoute},
      ],
      imports: [
        RouterTestingModule
      ]
    });
  });

  activatedRoute = new ActivatedRouteStub();

  it('should ...', inject([UrlRouteService], (service: UrlRouteService) => {
    expect(service).toBeTruthy();
  }));
});

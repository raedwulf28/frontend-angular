import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UrlRouteService {
    serviceData: string[];
    private subject = new Subject<any>();
constructor(public activatedRoute: ActivatedRoute,
    public router: Router) { }

url(): Observable<string> {
        this.serviceData = this.activatedRoute.snapshot.url.map(segment => segment.path).join('/').split('/');
        console.log(this.activatedRoute);
      this.subject.next({data: this.serviceData});
        return this.subject.asObservable();
}

inHomePage(): Observable<boolean> {
    this.serviceData = this.activatedRoute.snapshot.url.map(segment => segment.path).join('/').split('/');
  this.subject.next(true);
    return this.subject.asObservable();
}

outHomePage(): Observable<boolean> {
    this.serviceData = this.activatedRoute.snapshot.url.map(segment => segment.path).join('/').split('/');
  this.subject.next(false);
    return this.subject.asObservable();
}

}

/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import { AuthService, AuthServiceConfig } from 'angularx-social-login';


const STUB = [];
const AuthServiceConfigStub = new AuthServiceConfig(STUB);

describe('Service: Authentication', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthenticationService,
        AuthService,
        {provide: AuthServiceConfig, useValue: AuthServiceConfigStub}
      ]
    });
  });

  it('should ...', inject([AuthenticationService], (service: AuthenticationService) => {
    expect(service).toBeTruthy();
  }));
});

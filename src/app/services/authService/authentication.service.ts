import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import { AuthService } from 'angularx-social-login';

@Injectable()
export class AuthenticationService {
    private authManager_: BehaviorSubject<AuthState> = new BehaviorSubject(AuthState.LoggedOut);
    private authState_: AuthState;
    private loggedIn = false;
    authChange: Observable<AuthState>;

    constructor(public authService: AuthService) {
        this.authChange = this.authManager_.asObservable();
    }

    login(): void {
        this.setAuthState_(AuthState.LoggedIn);
    }

    logout(): void {
        this.setAuthState_(AuthState.LoggedOut);
        this.authService.authState.subscribe((user) => {
            if (user) {
                this.authService.signOut();
            }
          });
    }

    emitAuthState(): void {
        this.authManager_.next(this.authState_);
    }

    private setAuthState_(newAuthState: AuthState): void {
        this.authState_ = newAuthState;
        this.emitAuthState();
    }
}

export const enum AuthState {
LoggedIn,
LoggedOut
}

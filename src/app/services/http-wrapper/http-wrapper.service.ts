import {Injectable} from '@angular/core';
import {Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, XHRBackend, Response, Request} from '@angular/http';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/finally';

@Injectable()
export class HttpWrapperService extends Http {
  activeCalls: number;
  private subject = new Subject<any>();

  constructor(backend: XHRBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
    this.activeCalls = 0;
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    this.httpCallRequested();
    return super.get(url, options).finally(() => { this.httpCallReady(); });
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    this.httpCallRequested();
    return super.request(url, options).finally( () => { this.httpCallReady(); });
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    this.httpCallRequested();
    return super.post(url, body, options).finally( () => { this.httpCallReady(); } );
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return super.put(url, body, options);
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return super.delete(url, options);
  }

  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return super.patch(url, body, options);
  }

  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return super.head(url, options);
  }

  /**
   * Performs a request with `options` http method.
   */
  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    this.httpCallRequested();
    return super.options(url, options).finally(
      () => {
        this.httpCallReady();
      }
    );
  }

  private httpCallRequested(): void {
    this.activeCalls++;
    this.subject.next({activeCalls: this.activeCalls});
  }

  private httpCallReady(): void {
    this.activeCalls--;
    this.subject.next({activeCalls: this.activeCalls});
  }

  getMessage(): Observable<any> {
    this.subject.next({activeCalls: this.activeCalls});
    return this.subject.asObservable();
  }

}

export function HttpWrapperFactory(backend: XHRBackend,
  defaultOptions: RequestOptions) {
  return new HttpWrapperService(backend, defaultOptions);
}

import { TestBed, inject } from '@angular/core/testing';
import { BaseRequestOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { HttpWrapperService, HttpWrapperFactory } from './http-wrapper.service';

describe('HttpWrapperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpWrapperService,
        {
          provide: HttpWrapperService,
          useFactory: HttpWrapperFactory,
          deps: [MockBackend, BaseRequestOptions]
        },
        MockBackend,
        BaseRequestOptions,
      ]
    });
  });

  it('should be created', inject([HttpWrapperService], (service: HttpWrapperService) => {
    expect(service).toBeTruthy();
  }));
});

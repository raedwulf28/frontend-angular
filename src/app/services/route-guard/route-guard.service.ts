import { Injectable } from '@angular/core';
import { CanActivate, Router, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService, AuthState } from '../authService/authentication.service';

@Injectable()
export class RouteGuardService implements CanActivate, CanActivateChild {
    login = false;
    constructor(
      public authService_: AuthenticationService,
      public router: Router
    ) {
        authService_.authChange.subscribe(
            newAuthState => {
            this.login = (newAuthState === AuthState.LoggedIn);
             });
    }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.login) {
      return true;
    }
    this.router.navigate(['/login'], {
      queryParams: {
        return: state.url
      }
    });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

}

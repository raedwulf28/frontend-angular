/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RouteGuardService } from './route-guard.service';
import { AuthenticationService } from '../authService/authentication.service';
import { AuthService, AuthServiceConfig } from 'angularx-social-login';
import { RouterTestingModule } from '@angular/router/testing';

const STUB = [];
const AuthServiceConfigStub = new AuthServiceConfig(STUB);

describe('Service: RouteGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RouteGuardService,
        AuthenticationService,
        AuthService,
        {provide: AuthServiceConfig, useValue: AuthServiceConfigStub}
      ],
      imports: [
        RouterTestingModule
      ]
    });
  });

  it('should ...', inject([RouteGuardService], (service: RouteGuardService) => {
    expect(service).toBeTruthy();
  }));
});

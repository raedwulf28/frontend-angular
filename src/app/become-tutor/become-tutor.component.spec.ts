/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BecomeTutorComponent } from './become-tutor.component';
import { MatButtonModule, MatIconModule, MatInputModule, MatCheckboxModule, MatStepperModule, MatRadioModule } from '@angular/material';
import { MatFormFieldModule, MatNativeDateModule, MatDatepickerModule, MatSelectModule } from '@angular/material';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from '../services/data/Data.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('BecomeTutorComponent', () => {
  let component: BecomeTutorComponent;
  let fixture: ComponentFixture<BecomeTutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ BecomeTutorComponent ],
      providers: [
        FormBuilder,
        DataService],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        MatCheckboxModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatRadioModule,
        MatStepperModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BecomeTutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

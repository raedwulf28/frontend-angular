import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Job } from '../shared/models/job';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data/Data.service';

@Component({
  selector: 'app-become-tutor',
  templateUrl: './become-tutor.component.html',
  styleUrls: ['./become-tutor.component.scss']
})

export class BecomeTutorComponent implements OnInit {
  @ViewChild('stepper') stepper;
  generalFormGroup: FormGroup;
  profileFormGroup: FormGroup;
  subjectsFormGroup: FormGroup;
  aboutTutorFormGroup: FormGroup;
  resumeFormGroup: FormGroup;
  genders = ['Male', 'Female'];
  countrys = ['USA', 'COSTA RICA', 'PANAMA'];
  languages = ['Ingles', 'Español', 'Italiano', 'Frances'];
  tutoringAreas = ['Legal', 'Enginiering', 'Education', 'Health'];
  timeZones = ['Europe/London +00', 'Central America +6'];
  certificates = [];
  jobs: Job[] = [];
  minDate = new Date(1900, 0, 1);
  maxDate = new Date(2013, 0, 1);
  disabledEndDate = false;
  file: File;
  reader = new FileReader();
  constructor(
    private _formBuilder: FormBuilder,
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {

    /**
     * Validators for General Information Form Group
     */
    this.generalFormGroup = this._formBuilder.group({
      genderCtrl: ['', Validators.required],
      firstNCtrl: ['', Validators.required],
      lastNCtrl: ['', Validators.required],
      countryCtrl: ['', Validators.required],
      languageCtrl: ['', Validators.required],
      birthCtrl: ['', Validators.required],
      phoneCtrl: ['', Validators.required]
    });

    /**
     * Validators for Profile Description Form Group
     */
    this.profileFormGroup = this._formBuilder.group({
      languageCtrl: ['', Validators.required],
      titleCtrl: ['', Validators.required],
      descriptionCtrl: ['']
    });

    /**
     * Validators for Subjects Form Group
     */
    this.subjectsFormGroup = this._formBuilder.group({
      tutoringAreaCtrl: ['', Validators.required]
    });

    /**
     * Validators for About Tutor Form Group
     */
    this.aboutTutorFormGroup = this._formBuilder.group({
      experienceCtrl: ['', Validators.required],
      skypeCtrl: ['', Validators.required],
      timeZoneCtrl: ['', Validators.required],
      hourRateCtrl: ['', Validators.required],
      groupLessonCtrl: ['', Validators.required]
    });

    /**
     * Validators for Resume Form Group
     */
    this.resumeFormGroup = this._formBuilder.group({
      videoCtrl: ['', Validators.required],
      instituteCtrl: ['', Validators.required],
      degreeCtrl: ['', Validators.required],
      certificateCtrl: ['', Validators.required],
      jobPositionCtrl: ['', Validators.required],
      jobCompanyCtrl: ['', Validators.required],
      jobStartCtrl: ['', Validators.required],
      jobEndCtrl: []
    });

    this.route
      .queryParams
        .subscribe(params => {
          console.log(this.dataService.serviceData);
          if (params['editionFromResult'] && this.dataService.serviceData) {
            this.generalFormGroup.patchValue({
              firstNCtrl: this.dataService.serviceData.firstName,
            });
          }


        });


  }

  next() {
    window.scrollTo(0, 0);
  }

  back() {
    window.scrollTo(0, 0);
  }

  toggleEndDate() {
    this.disabledEndDate = !this.disabledEndDate;
    const ctrl = this.resumeFormGroup.get('jobEndCtrl');
    if (this.disabledEndDate === false) {
      ctrl.enable();
    } else {
      ctrl.disable();
    }
  }

  addCertification() {
    if (this.resumeFormGroup.value.certificateCtrl) {
      this.certificates.push(this.resumeFormGroup.value.certificateCtrl);
    }
  }

  addJobs() {
    console.log(this.resumeFormGroup.value.jobEndCtrl - this.resumeFormGroup.value.jobStartCtrl);
    if (this.resumeFormGroup.value.jobCompanyCtrl && this.resumeFormGroup.value.jobPositionCtrl) {
      this.jobs.push({
        company: this.resumeFormGroup.value.jobCompanyCtrl,
        position: this.resumeFormGroup.value.jobPositionCtrl,
        startTime: this.resumeFormGroup.value.jobStartCtrl,
        endTime: this.resumeFormGroup.value.jobEndCtrl
      });
    }
  }

  watchResult(): void {
    this.dataService.serviceData = {
      name: this.generalFormGroup.get('firstNCtrl').value + ' ' + this.generalFormGroup.get('lastNCtrl').value,
      city: this.generalFormGroup.get('countryCtrl').value,
      tutorIn: this.subjectsFormGroup.get('tutoringAreaCtrl').value,
      speakIn: this.profileFormGroup.get('languageCtrl').value,
      payRate: this.aboutTutorFormGroup.get('hourRateCtrl').value,
      tutorDescription: this.profileFormGroup.get('descriptionCtrl').value,
      certifications: this.certificates,
      workHistory: this.jobs,
      education: { institute: this.resumeFormGroup.get('instituteCtrl').value,
                    degree: this.resumeFormGroup.get('degreeCtrl').value},
      video: this.resumeFormGroup.get('videoCtrl').value,
      image: this.reader.result
    };
    this.router.navigate(['/test'], {queryParams: {editionFromForm: true}});
  }

  fileChange(file) {
    this.file = file.target.files[0];
    this.accountImgPath(this.file);
  }

  accountImgPath(file) {
    if (file) {
      this.reader.readAsDataURL(file);
      return this.reader.result;
    } else if (!this.file) {
      return '../../assets/account.png';
    } else {
      return this.reader.result;
    }

  }
}
